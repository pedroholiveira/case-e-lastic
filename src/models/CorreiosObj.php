<?php

namespace src\models;

class CorreiosObj
{

	public function getJson(string $obj)
	{
		$post = array('Objetos' => $obj);
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "https://www2.correios.com.br/sistemas/rastreamento/resultado_semcontent.cfm");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post));
		$output = curl_exec($ch);
		curl_close($ch);


		$out = explode("table class=\"listEvent sro\">", $output);

		if (isset($out[1])) {

			$output = explode("<table class=\"listEvent sro\">", $output);
			$output = explode("</table>", $output[1]);
			$output = str_replace("</td>", "", $output[0]);
			$output = str_replace("</tr>", "", $output);
			$output = str_replace("<strong>", "", $output);
			$output = str_replace("</strong>", "", $output);
			$output = str_replace("<tbody>", "", $output);
			$output = str_replace("</tbody>", "", $output);
			$output = str_replace("<label style=\"text-transform:capitalize;\">", "", $output);
			$output = str_replace("</label>", "", $output);
			$output = str_replace("&nbsp;", "", $output);
			$output = str_replace("<td class=\"sroDtEvent\" valign=\"top\">", "", $output);
			$output = explode("<tr>", $output);


			$novo_array = array();

			foreach ($output as $texto) {

				$info   = explode("<td class=\"sroLbEvent\">", $texto);
				$data  = explode("<br />", $info[0]);

				$day   = trim($data[0]);
				$hour  = trim(@$data[1]);
				$location = trim(@$data[2]);

				$data = explode("<br />", @$info[1]);
				$action  = trim($data[0]);

				$exAction   = explode($action . "<br />", @$info[1]);
				$acrionMsg  = strip_tags(trim(preg_replace('/\s\s+/', ' ', $exAction[0])));

				if ("" != $day) {

					$exploDate = explode('/', $day);
					$day1 = $exploDate[2] . '-' . $exploDate[1] . '-' . $exploDate[0];
					$day2 = date('Y-m-d');

					$difference = strtotime($day2) - strtotime($day1);
					$days = floor($difference / (60 * 60 * 24));

					$change = utf8_encode("há {$days} days");


					$novo_array[] = array("date" => $day, "hour" => $hour, "location" => $location, "action" => utf8_encode($action), "message" => utf8_encode($acrionMsg), "change" => utf8_decode($change));
				}
			}

			$jsonObcject = (object) $novo_array;
			header('Content-type: application/json');
			$json = json_encode($jsonObcject);
			return $json;
		} else {

			$jsonObcject = new CorreiosObj();
			$jsonObcject->error = true;
			$jsonObcject->msg = "Objeto não encontrado";
			header('Content-type: application/json');
			$json = json_encode($jsonObcject);
			return $json;
		}
	}
}
