<?php

namespace src\models;

use Dompdf\Dompdf;
use Dompdf\Options;

class Pdf
{
    /** @var Dompdf */
    private $dompdf;
    
    public function __construct()
    {
        $options = new Options();
        $options->set('isRemoteEnabled', TRUE);
        $this->dompdf = new Dompdf($options);
        $this->dompdf->setPaper("A4");
    }

    public function createPDF(string $content)
    {
        $this->dompdf->loadHtml($content);
        $this->dompdf->render();
        $content = $this->dompdf->output();
        file_put_contents("./temp/file.pdf", $content);
    }
}
