<?php

namespace src\controllers;

use src\models\Pdf;

class PdfController
{
    public function newPdf($content)
    {
        $pdf = new Pdf();
        $pdf->createPDF($content);
    }
}
