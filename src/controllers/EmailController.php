<?php

namespace src\controllers;

use src\models\Email;

class EmailController
{
    public function sendEmail($subject,  $body, $recipient_name, $recipient_email, $filePath, $fileName)
    {
        $email = new Email();
        $email->add($subject, $body, $recipient_name, $recipient_email)->attach($filePath, $fileName)->send();
    }
}
