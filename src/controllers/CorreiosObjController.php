<?php

namespace src\controllers;

use src\models\CorreiosObj;

class CorreiosObjController
{
	public function getData($obj)
	{
		$json = new CorreiosObj();
		return json_decode($json->getJson($obj));
	}
}
