<?php

require "../vendor/autoload.php";

use src\controllers\PdfController;
use src\controllers\CorreiosObjController;
use src\controllers\EmailController;
use Pelago\Emogrifier\CssInliner;

$correiosObj = new CorreiosObjController();
$email = new EmailController();
$pdf = new PdfController();


if (isset($_GET) || isset($_POST)) {
    $_POST = json_decode(file_get_contents('php://input'), true);
    $request = empty($_GET) ? $_POST : $_GET;
    $obj = isset($request['obj']) ? $request['obj'] : false;
    $userEmail = isset($request['email']) ? $request['email'] : false;

    if ($obj == false || $userEmail == false) {
        return http_response_code(400);
    }

    $json = $correiosObj->getData($obj);

    if ($json->error) {
        return http_response_code(204);
    }

    $html = file_get_contents("template/template.html");
    $data = file_get_contents("template/content.html");
    $css  = file_get_contents("template/styles.css");

    $replace = ['{hour}', '{date}', '{location}', '{message}'];
    $action = array();

    foreach ($json as $key => $obj) {
        $info = [$obj->hour, $obj->date, $obj->location, $obj->message];
        $htmlContent .= str_replace($replace, $info, $data);
        array_push($action, $obj->action);
    }

    $subject = $action[0];

    $html = str_replace('{content}', $htmlContent, $html);

    $visualHtml = CssInliner::fromHtml($html)->inlineCss($css)->render();

    $pdf->newPdf($visualHtml);

    $email->sendEmail(
        $subject,
        $visualHtml,
        "E-lastic",
        $userEmail,
        "temp/file.pdf",
        "Rastreio.pdf"
    );

    return http_response_code(200);
}
