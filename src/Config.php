<?php

$dotenv = Dotenv\Dotenv::createImmutable(__DIR__ . '/..');
$dotenv->load();

define("MAIL", [
    "host" => "smtp.gmail.com",
    "port" => "587",
    "user" => $_ENV['USER'],
    "password" => $_ENV['PASSWORD'],
    "from_name" => $_ENV['FROM_NAME'],
    "from_email" => $_ENV['FROM_EMAIL']
]);