# Case E-lastic

### Proposta

*Resolução do desafio proposto pela E-lastic Startup Healthtech*

O projeto consiste em uma aplicação que pegue os dados de uma API dos correios de um produto a ser entregue e os envie por e-mail juntamente com um pdf contendo as mesmas informações.

## Aplicação

A aplicação foi feita em PHP na versão 7.2.5, utilizando o composer para o gerenciamento das libs de terceiros.

## Pré-requisitos para o setup:

- PHP ^7.2.5
- composer

### Instalação
1. Clone o repositório para o seu ambiente de desenvolvimento.
```
git clone git@gitlab.com:pedroholiveira/case-e-lastic.git
```

2. Crie os arquivos das variaveis de ambiente a partir do arquivo `.env.example` . Se estiver usando um terminal linux utilize o comando para copiar:
```
cp .env.example .env
```

3. Abra o arquivo `.env` com um editor de texto e altere as variáveis de ambiente. Nesse arquivo será definido as informações referentes ao e-mail.


5. Agora execute o comando de instação do composer para a instação das bibliotecas.
```
php composer.phar install
```

6. Suba um servidor php dentro da pasta do projeto com o comando:
```
php -S localhost:8080
```

### Usabilidade

Para usar a aplicação basta mandar uma requisição contendo o código de rastreio e o e-mail para onde os dados devem ser enviados. A aplicação funciona tanto com o método GET quanto no método POST. 

O endereço padrão para enviar as requisições por GET é:
```
http://localhost:8080/src/index.php?obj={CÓDIGO-DE-RASTREIO}&email={EMAIL}
```
Basta substituir os textos entre chaves. Exemplo:
```
http://localhost:8080/src/index.php?obj=OA016913717BR&email=pdr_castro@email.com
```

Para acessar via POST envie uma requisição passando um json no formato:
```
{
	"obj" : "CÓDIGO-DE-RASTREIO",
	"email" : "EMAIL"
}
```

Basta substituir os textos entre chaves. Exemplo:
```
{
	"obj" : "OA016913717BR",
	"email" : "pdr_castro@email.com"
}
```

## Referências

PHP

* [PHP](http://php.net/docs.php)
* [PHP do jeito certo](http://br.phptherightway.com/)

CSS

* [CSS](https://developer.mozilla.org/en-US/docs/Learn/CSS)
* [CSS Tricks](https://css-tricks.com/)

GIT

* [GIT Reference](https://git-scm.com/docs/)

Libs de terceiros

* [PHPMailer](https://github.com/PHPMailer/PHPMailer)
* [Dompdf](https://github.com/dompdf/dompdf)
* [PHP dotenv](https://github.com/vlucas/phpdotenv)
* [Emogrifer - CSS inliner](https://github.com/MyIntervals/emogrifier)
